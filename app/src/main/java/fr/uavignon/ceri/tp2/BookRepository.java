package fr.uavignon.ceri.tp2;

import android.app.Application;
import android.os.AsyncTask;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import fr.uavignon.ceri.tp2.data.Book;

import static fr.uavignon.ceri.tp2.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {
    private MutableLiveData<List<Book>> selectedBook =
            new MutableLiveData<>();
    private LiveData<List<Book>> allBooks;

    public BookDao  bookDao;

    public BookRepository(Application application) {
        BookRoomDatabase db =BookRoomDatabase.getDatabase(application);
        bookDao = db.BookDao();
        allBooks = bookDao.getAllBooks();
    }

    public void insertBook(Book book) {
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(book);
        });
    }



    public void deleteBook(long id) {
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteBook(id);
        });
    }



    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public MutableLiveData<List<Book>> getSearchResults() {
        return selectedBook;
    }


}
