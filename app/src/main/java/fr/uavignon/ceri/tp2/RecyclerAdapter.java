package fr.uavignon.ceri.tp2;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.tp2.RecyclerAdapter.ViewHolder> {

    private int bookItemLayout;
    private List<Book> bookList;

    public RecyclerAdapter(int layoutId) {
        bookItemLayout = layoutId;
    }

    public void setBookList(List<Book> books) {
       bookList = books;
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(bookItemLayout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }
    @NonNull
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {
        TextView item = holder.item;
        item.setText(bookList.get(listPosition).getTitle());

    }

    @Override
    public int getItemCount() {
        return bookList == null ? 0 : bookList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
       TextView item;


        ViewHolder(View itemView) {
            super(itemView);
            item = itemView.findViewById(R.id.book_row);


         /*   itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    int position = getAdapterPosition();
                    long id = RecyclerAdapter.this.bookList.get((int)getAdapterPosition()).getId() ;
                     Snackbar.make(v, "Click detected on chapter " + (position+1),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();


                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setBookNum(position);
                    Navigation.findNavController(v).navigate(action);


                }
            });*/

        }
    }

}