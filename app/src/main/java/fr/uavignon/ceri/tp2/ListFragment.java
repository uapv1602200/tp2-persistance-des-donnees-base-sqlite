package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Locale;

import fr.uavignon.ceri.tp2.data.Book;

public class ListFragment extends Fragment {

    private ListViewModel viewModel;
    private RecyclerAdapter adapter;
    
    private TextView bookTitle;
    private TextView bookAuthor;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(ListViewModel.class);

        bookTitle = getView().findViewById(R.id.bookTitle);
        bookAuthor = getView().findViewById(R.id.bookAuthor);

        observerSetup();
        recyclerSetup();
    }
        private void observerSetup() {
            viewModel.getAllBooks().observe(getViewLifecycleOwner(),
                    new Observer<List<Book>>() {
                        @Override
                        public void onChanged(@Nullable final List<Book> books) {
                            adapter.setBookList(books);
                        }

          /* viewModel.getSearchResults().observe(getViewLifecycleOwner(),
                    new Observer<List<Book>>() {
                        @Override
                        public void onChanged(@Nullable final List<Book> books) {

                            if (books.size() > 0) {
                                //bookId.setText(String.format(Locale.US, "%d",
                                 //books.get(0).getId()));
                                bookTitle.setText(books.get(0).getTitle());
                                bookAuthor.setText(String.format(Locale.US, "%d",
                                        books.get(0).getAuthors()));
                            }
                        }*/

                    });
        }

                        private void recyclerSetup() {
                            RecyclerView recyclerView;
                            adapter = new RecyclerAdapter(R.layout.book_list_item);
                            recyclerView = getView().findViewById(R.id.book_recycler);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                            recyclerView.setAdapter(adapter);
                        }



}