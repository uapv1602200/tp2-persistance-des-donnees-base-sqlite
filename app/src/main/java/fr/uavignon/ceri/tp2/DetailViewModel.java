package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailViewModel extends AndroidViewModel
{
    private BookRepository repository;

    private MutableLiveData<List<Book>> searchResults;
    public DetailViewModel (Application application) {
        super(application);
        repository = new BookRepository(application);
        searchResults = repository.getSearchResults();

    }
    MutableLiveData<List<Book>> getSearchResults() {
        return searchResults;
    }

}

